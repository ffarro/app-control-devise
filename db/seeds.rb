# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Employee.destroy_all
User.destroy_all
Register.destroy_all


employee = Employee.create([{ 
    name: 'Gabriel',
    paternalLastName: 'García',
    maternalLastName: 'Marquez',
    state: true
}, 
{ 
    name: 'Lu',
    paternalLastName: 'Godoy',
    maternalLastName: 'Alcayaga',
    state: true
}
])

User.create([{ 
    email: 'ffarro@gmail.com',
    password: '123456',
    password_confirmation:'123456'
}
])


30.times do |i|
    Register.create(   
        date: "#{i+1}-09-2021",
        hour_start: ("#{i+1}-09-2021 8:00"),
        hour_end: ("#{i+1}-09-2021 13:00"),
        total_hours: 5,
        employee_id: 1)
end


30.times do |i|
    Register.create(   
        date: "#{i+1}-09-2021",
        hour_start: ("#{i+1}-09-2021 8:00"),
        hour_end: ("#{i+1}-09-2021 14:00"),
        total_hours: 6,
        employee_id: 2)
end

