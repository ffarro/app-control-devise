class CreateRegisters < ActiveRecord::Migration[5.2]
  def change
    create_table :registers do |t|
      t.date :date
      t.time :hour_start
      t.time :hour_end
      t.float :total_hours
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
