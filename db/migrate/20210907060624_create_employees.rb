class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :paternalLastName
      t.string :maternalLastName
      t.boolean :state

      t.timestamps
    end
  end
end
