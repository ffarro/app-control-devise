Rails.application.routes.draw do
  get 'main/index'

  get 'reports', to:'report#index'
  get 'report/index'
  get 'report/exportar'
  post 'report/exportar'
  
  get 'calendar/index'
  resources :registers
  resources :employees
  devise_for :views
  devise_for :users
  root to: 'main#index'
  get 'home/index'
  #root to: 'home#index'

  post 'employees/up'
  post 'employees/down'
  
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
