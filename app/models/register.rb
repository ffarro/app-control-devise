class Register < ApplicationRecord
  belongs_to :employee

  def self.to_csv
    CSV.generate do |csv|
      csv << ["ID", "Empleado", "Fecha", "Hora de entrada", "Hora de salida", "Total de horas de trabajo"]
      total = 0
      all.each do |r|
        if r.hour_end != nil 
          total += r.total_hours
        end
        csv << [
          r.id, "#{r.employee.full_name}", r.date, 
          r.hour_start.strftime("%I:%M %p"), r.hour_end == nil ? 'Aún trabajando':r.hour_end.strftime("%I:%M %p"), 
          r.total_hours ]
        
      end
      csv << []
      csv << ["Total de horas trabajadas: ", total]
    end
  end

  validates :date, uniqueness: { scope: [:employee_id] }  
end
