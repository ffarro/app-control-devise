class Employee < ApplicationRecord
    has_many_attached :documents #Un empleado => tiene varios documentos

    def full_name
        "#{self.try(:name)} #{self.try(:paternalLastName)} #{self.try(:maternalLastName)}"
    end

    def search_name(search)
        Employee.with_attached_documents.references(:documents_attachments).
          where(ActiveStorage::Blob.arel_table[:filename].matches("%#{search}%"))
    end
    
end
