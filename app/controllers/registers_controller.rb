class RegistersController < ApplicationController
  skip_before_action :authenticate_user!, :only => [:new, :show, :create]

  before_action :set_register, only: %i[ show edit update destroy ]
  
  layout "free", only: [:new, :show, "errores"]


  # GET /registers or /registers.json
  def index
    @registers = Register.all
  end

  # GET /registers/1 or /registers/1.json
  def show
  end


  # GET /registers/new
  def new
    @register = Register.new
    @employees = Employee.all
  end

  # GET /registers/1/edit
  def edit
  end

  def errores
    render layout: false
  end

  # POST /registers or /registers.json
  def create
      @er = Array.new
      
      #FECHA
      @day = register_params["date(1i)"].to_i 
      @month = register_params["date(2i)"].to_i
      @year = register_params["date(3i)"].to_i
      @date = Date.new(@day, @month, @year)
     
      #HORA 
      @hour = register_params["hour(4i)"].to_i 
      @seg = register_params["hour(5i)"].to_i
      @time = Time.zone.local(@day,@month,@year, @hour, @seg, 0)

      puts "#{@date}"
      puts "#{@time}"

      @registers = Register.where( employee_id: register_params[:employee_id] , date: @date.all_day)
      
      
      if @registers.length == 0
        if register_params[:tipo] == "S"
          msj =  "Primero debes registrar tu entrada."
          @er = msj
          puts "#{msj}"
        else
          puts "MUY BIEN"
            @register = Register.new
            @register.date = @date
            @register.hour_start = @time
            @register.hour_end = nil
            @register.total_hours = nil
            @register.employee_id = register_params[:employee_id]
        end
      else  
        
        if register_params[:tipo] == "E"
          msj = "La hora de entrada ya ha sido registrada, registre su hora de salida."
          @er = msj
          puts "#{msj}"
        else
          @register = @registers[0]
                  
          if @time <= @register.hour_start
              msj = "La hora de salida no puede ser menor a la hora de entrada"
              @er = msj

              puts "#{msj}"
          else
              puts "REgistrando la salida"
              @register.hour_end = @time
              @register.hour_start = @register.hour_start 
              @register.total_hours = (Time.parse(@register.hour_end.strftime("%Y-%m-%d %H:%M")) - Time.parse(@register.hour_start.strftime("%Y-%m-%d %H:%M")))/3600
          end
        end

      end

      respond_to do |format|
        if @register == nil
          format.html {render "registers/errores.html.erb",:layout => 'nil' }
        elsif @register.save
          puts "AGREDADO"
          format.html { redirect_to @register, :layout=>'nil', notice: "Register was successfully created." }
          format.json { render :show, status: :created, location: @register }
        end
      end
  end

  # PATCH/PUT /registers/1 or /registers/1.json
  def update
    respond_to do |format|
      if @register.update(register_params)
        format.html { redirect_to @register, notice: "Register was successfully updated." }
        format.json { render :show, status: :ok, location: @register }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @register.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registers/1 or /registers/1.json
  def destroy
    @register.destroy
    respond_to do |format|
      format.html { redirect_to registers_url, notice: "Register was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_register
      @register = Register.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def register_params
      #params.require(:register).permit(:date, :hour_start, :hour_end, :total_hours, :employee_id)
      params.require(:register).permit(:tipo,:hour,:date , :employee_id)
    end
end
