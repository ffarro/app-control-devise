class MainController < ApplicationController
  skip_before_action :authenticate_user!, :only => [:index]
  layout "main"

  def index
  end

end
