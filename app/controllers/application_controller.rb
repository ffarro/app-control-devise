class ApplicationController < ActionController::Base
  before_action :authenticate_user!
    layout :nuevo

  private

  def nuevo
    if devise_controller?
      "devise"
    else
      "application"
    end
  end

end
