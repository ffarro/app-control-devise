class ReportController < ApplicationController
  before_action :authenticate_user!

  def index
    @years = (2018..2021)
    @people = Employee.all
    @months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ]
  end

  def exportar
    #Variables
    @month = Integer(params[:month_id])
    @year = Integer(params[:year_id])
    @person_id = Integer(params[:person_id])
    @range = Date.new(@year, @month)
    @total = 0 #total de horas trabajas del empleado
   
    @registers =  Register.where( employee_id: @person_id, date: @range.all_month) 
    
    #Número total de horas de trabajo del empleado
    @registers.each do |r|
      if r.hour_end != nil
        @total += r.total_hours
      end
    end 

    #Empleado 
    @employee = Employee.find(@person_id) 
    existen = @employee.documents.attached?

    band = true
    formato = request.format.symbol

    @formatos_almacenados = Array.new

    if @registers.length > 0 # Sí hay resultados de búsqueda
      file = "employee-#{@person_id}_#{@month}_#{@year}"
      @docs = @employee.documents.references(:blobs).joins(:blob).where("active_storage_blobs.filename LIKE '%#{file}%' ")
      
      if existen &&  @docs.length > 0 
        #HAY REPORTES GUARDADOS
        @docs = @docs

        @docs.each_with_index do |file, index|
          name = (file.filename).to_s.split('.')
          @formatos_almacenados.push(name[1])
        end
      else
        band = false
      end
    end


    if params[:format]  == 'xls'
      template = "report/exportar.xls.erb"
      iostr2 = StringIO.new(render_to_string(template: template,layout: false, formats: [:xls], locals: { :@registers => @registers, total: 0  })) 
      
      if band == false
        @employee.documents.attach(io: iostr2,filename: "#{file}.#{formato}")
        puts "Este reporte XLS se acaba de agregar al active storage"
      else 
        if @formatos_almacenados.include?('xls')
          puts "Ya existe este reporte XLS en el active storage"
        else
          @employee.documents.attach(io: iostr2,filename: "#{file}.#{formato}")
          puts "Este reporte se acaba de agregar al active storage"
        end
      end

    elsif params[:format] == 'csv'
      
      iostr3 = StringIO.new(send_data @registers.to_csv, type: 'text/csv; charset=utf-8' ) 

      if band == false
        @employee.documents.attach(io: iostr3,filename: "#{file}.#{formato}")
        puts "Este reporte se acaba de agregar al active storage"
      else
        if @formatos_almacenados.include?('csv')
          puts "Ya existe este reporte CSV en el active storage"
        else
          @employee.documents.attach(io: iostr3,filename: "#{file}.#{formato}")
          puts "Este reporte se acaba de agregar al active storage"
        end
      end
    
    end

   

  end

end
