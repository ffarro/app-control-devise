json.extract! register, :id, :date, :hour_start, :hour_end, :total_hours, :employee_id, :created_at, :updated_at
json.url register_url(register, format: :json)
